This is the Artifact for SC19 submission
Each folder has a seperate makefile which may require CUDA libraries and drivers to compile
All the code was tested on V100 with NVLink2 and V100 with PCIE 
############################################
The bandwidth folder cover the code for bandwidth test present in the paper.
The prototype folder contains our prototype implementations discussed in the paper.
############################################
Most Macros value have been pre-defined. Some might have hints to change to fit different environments (NVLink or PCIE) to achieve best performance
